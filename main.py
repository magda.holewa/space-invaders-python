import pyglet
from scripts import gameobject, resources, player, alien, aliens, bullet, shield, menu, textdata, powerup
from enum import Enum

class State(Enum):
	RUN = 0
	MENU = 1
	LOST = 2
	WIN = 3
	PAUSED = 4
	LOAD = 5

"""
tworzenie menu
"""
gameState = State.MENU
gameWindow = pyglet.window.Window(800, 600)
textdata.loadScores()
menuobj = menu.Menu(gameWindow)
menubcg = pyglet.sprite.Sprite(img=resources.titleImage, x=0, y=0)

"""
deklaracje obiektow - grafika, tekst
"""

batch = pyglet.graphics.Batch()
background = pyglet.sprite.Sprite(img=resources.backImage, x=0, y=0, batch=batch)
stripe = pyglet.sprite.Sprite(img=resources.stripeImage, x=0, y=gameWindow.height - 260)
labelBatch = pyglet.graphics.Batch()

lostBatch = pyglet.graphics.Batch()
lostLabel = pyglet.text.Label('You lost! Press ENTER to return to menu.', font_size = 20, x = gameWindow.width//2, y = gameWindow.height - 250, anchor_x='center', batch=lostBatch)
winBatch = pyglet.graphics.Batch()
winLabel = pyglet.text.Label('You won! Press ENTER to return to menu.', font_size = 20, x = gameWindow.width//2, y = gameWindow.height - 250, anchor_x='center', batch=winBatch)
pauseBatch = pyglet.graphics.Batch()
pauseLabel = pyglet.text.Label('PAUSED. Press P to continue.', font_size = 20, x = gameWindow.width//2, y = gameWindow.height - 250, anchor_x='center', batch=pauseBatch)

livesLabel = pyglet.text.Label('Lives: 3', font_size = 20, x = 20, y = gameWindow.height - 30, batch=labelBatch)
levelLabel = pyglet.text.Label('Level: 1', font_size = 20, x = gameWindow.width - 150, y = gameWindow.height - 30, batch=labelBatch)
scoreLabel = pyglet.text.Label('Score: 0', font_size = 20, x = gameWindow.width / 2 - 50, y = gameWindow.height - 30, batch=labelBatch)
loadLabel = pyglet.text.Label('Loading level 1', font_size = 20, x = gameWindow.width//2, anchor_x='center', y = gameWindow.height - 250)
loadTime = 1.0

player = player.Player(x=400, y=10, batch=batch)
enemy = None
shields = []
powerups = []
level = 0

"""
obsluga muzyki (sound dla efektow, music dla tla)
"""
sound = pyglet.media.Player()
sound.play()

musicgroup = pyglet.media.SourceGroup(resources.track.audio_format, None)
musicgroup.queue(resources.track)
musicgroup.loop = True
music = pyglet.media.Player()
music.queue(musicgroup)
music.play()

catchInput = False

@gameWindow.event
def on_key_press(symbol, modifiers):
	global gameState
	global catchInput
	if gameState == State.RUN:
		if catchInput:
			catchInput = False
		if symbol == pyglet.window.key.D or symbol == pyglet.window.key.RIGHT:
			player.velocityX += player.speed
		elif symbol == pyglet.window.key.A or symbol == pyglet.window.key.LEFT:
			player.velocityX -= player.speed
		elif symbol == pyglet.window.key.SPACE:
			if player.shoot():
				sound.queue(resources.beam)
				sound.play()
		elif symbol == pyglet.window.key.P:
			gameState = State.PAUSED
			player.velocityX = 0.0
	elif gameState == State.MENU:
		if menuobj.state == menu.MState.SUB:
			if symbol == pyglet.window.key.ENTER:
				menuobj.activate()
		else:
			if symbol == pyglet.window.key.ENTER:
				option = menuobj.activate()
				if option == 2:
					gameState = State.RUN
					setLevel(textdata.loadGame())
				elif option == 1:
					gameState = State.RUN
					nextLevel(True)
					textdata.saveGame(0, 0, 3)
				elif option == -1:
					print("Exiting...")
					pyglet.app.exit()
			elif symbol == pyglet.window.key.UP:
				menuobj.opt[menuobj.activeOption].color = (255,255,255,255)
				if menuobj.activeOption == 0:
					menuobj.activeOption = len(menuobj.opt) - 1
				else:
					menuobj.activeOption -= 1;
				menuobj.opt[menuobj.activeOption].color = (150, 150, 255, 255)
			elif symbol == pyglet.window.key.DOWN:
				menuobj.opt[menuobj.activeOption].color = (255,255,255,255)
				if menuobj.activeOption == len(menuobj.opt) - 1:
					menuobj.activeOption = 0
				else:
					menuobj.activeOption += 1;
				menuobj.opt[menuobj.activeOption].color = (150, 150, 255, 255)
	elif gameState == State.PAUSED:
		if symbol == pyglet.window.key.P:
			catchInput = True
			gameState = State.RUN
	else:
		if symbol == pyglet.window.key.ENTER:
			gameState = State.MENU
			#nextLevel(True)

@gameWindow.event
def on_key_release(symbol, modifiers):
	if gameState == State.RUN and not catchInput:
		if symbol == pyglet.window.key.D or symbol == pyglet.window.key.RIGHT:
			player.velocityX -= player.speed
		elif symbol == pyglet.window.key.A or symbol == pyglet.window.key.LEFT:
			player.velocityX += player.speed

@gameWindow.event
def on_draw():
	if gameState == State.MENU:
		menubcg.draw()
		if menuobj.state == menu.MState.MAIN:
			menuobj.mainBatch.draw()
		else:
			menuobj.subBatch.draw()
			menuobj.backButton.draw()
	else:
		batch.draw()
		labelBatch.draw()
		if gameState == State.LOAD:
			stripe.draw()
			loadLabel.draw()
		elif gameState == State.LOST:
			stripe.draw()
			lostBatch.draw()
		elif gameState == State.WIN:
			stripe.draw()
			winBatch.draw()
		elif gameState == State.PAUSED:
			stripe.draw()
			pauseBatch.draw()

def setLevel(arr, newShield=True):
	global level
	global enemy
	global shields

	enemy = aliens.Aliens(resources.levels[int(arr[0])], batch)
	levelLabel.text = 'Level: ' + str(int(arr[0])+1)
	level = int(arr[0])
	if level >= 3:
		enemy.ai = True
	player.score = int(arr[1])
	player.lives = int(arr[2])
	player.ammo = []
	player.alive = True
	player.shootTimer = 0.0
	player.velocityX = 0.0
	if newShield:
		shields = []
		for i in range(20):
			shields.append(shield.Shield(x=70*(i//5 + 1)+20*i, y=70, batch=batch))

def nextLevel(newGame = False):
	global gameState
	global loadTime
	if newGame:
		player.alive = True
		if not player.batch:
			player.batch = batch
		setLevel(('0', '0', '3'))
	elif level < len(resources.levels) - 1:
		setLevel((str(level+1), player.score, player.lives), False)
		textdata.saveGame(level, player.score, player.lives)
		gameState = State.LOAD
		loadLabel.text = 'Loading level '+str(level+1)
		loadTime = 1.0
	else:
		gameState = State.WIN
		textdata.addScore(player.score)


def update(dt):
	global gameState
	global enemy
	global loadTime
	global catchInput
	if gameState == State.LOAD:
		loadTime -= dt
		if loadTime <= 0:
			gameState = State.RUN
			catchInput = True
		for p in powerups:
			p.update(dt, player, powerups, enemy, shields)
	elif gameState == State.RUN:
		for b in player.ammo[:]:
			for a in enemy.array:
				if a.alive:
					score = b.doCollision(a)
					if score:
						player.score += score
						a.spawnPowerUp(powerups)
			for s in shields:
				if s.alive and b.checkCollision(s):
					s.power -= 1
					b.alive = False
			score = b.doCollision(enemy.ufo)
			if score:
				player.score += score
				enemy.ufo.spawnPowerUp(powerups)
			b.update(dt)
			if not b.alive:
				player.ammo.remove(b)
		for b in enemy.ammo[:]:
			b.doCollision(player)
			for s in shields:
				if s.alive and b.checkCollision(s):
					s.power -= 1
					b.alive = False
			b.update(dt)
			if not b.alive:
				enemy.ammo.remove(b)
		if not (player.update(dt) and enemy.update(dt, powerups, player.x + player.width/2)):
			gameState = State.LOST
			textdata.saveGame(0, 0, 3)
			textdata.addScore(player.score)
		for p in powerups:
			p.update(dt, player, powerups, enemy, shields)
		next = True
		for a in enemy.array:
			if a.alive:
				next = False
				break
		if next:
			nextLevel()
		livesLabel.text = 'Lives: ' + str(player.lives)
		scoreLabel.text = 'Score: ' + str(player.score)
		for s in shields:
			if s.alive:
				s.update(dt)

if __name__ == '__main__':
	pyglet.clock.schedule(update)
	pyglet.app.run()
