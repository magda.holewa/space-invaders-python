from . import resources
from . import gameobject, powerup
import random
random.seed()

class Alien(gameobject.GameObject):
	def __init__(self, x, y, batch, value=1, moveDir=1):
		super(Alien, self).__init__(img=resources.alienImage, x=x, y=y, batch=batch)
		self.velocityX = 50.0 * moveDir
		self.value = value * 10
		if value == 1:
			self.color = (96, 250, 59)
		elif value == 2:
			self.color = (255, 200, 50)
		elif value == 3:
			self.color = (150, 230, 255)
	
	def update(self, dt, moveDown=False, moveForward=False, changeDir=False):
		if changeDir:
			self.velocityX = -self.velocityX
		if not self.alive:
			self.batch = None
		elif not powerup.PowerUp.activePowers[powerup.Powers.FREEZE.value]:
			if self.value == 40:
				if self.x < 10:
					self.velocityX = -self.velocityX
					self.x = 10
				if self.x + self.width > 790:
					self.velocityX = -self.velocityX
					self.x = 790 - self.width
				self.x += self.velocityX * dt
			else:
				if moveDown:
					self.y -= 50
					if self.y < 80:
						return True
				if moveForward:
					self.x += self.velocityX
		return False

	def spawnPowerUp(self, powerups):
		if self.value == 40 or not random.randrange(20):
			chance = random.randrange(7)
			if chance < 1:
				powerups.append(powerup.PowerUp(powerup.Powers.FREEZE, self.x, self.y, self.batch))
			elif chance < 3:
				powerups.append(powerup.PowerUp(powerup.Powers.LIFE, self.x, self.y, self.batch))
			elif chance < 5:
				powerups.append(powerup.PowerUp(powerup.Powers.SPEEDUP, self.x, self.y, self.batch))
			else:
				powerups.append(powerup.PowerUp(powerup.Powers.BONUS, self.x, self.y, self.batch))
