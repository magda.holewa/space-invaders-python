from . import alien, bullet, redalien, powerup
import math
import random

class Aliens:
	def __init__(self, level, batch):
		self.normal = level[0]
		self.ai = False
		self.array = []
		self.ammo = []
		self.ufo = redalien.RedAlien(batch)
		self.timeToShoot = 2.0
		self.activeColumns = []
		
		if self.normal:
			self.timeToMoveDown = 10.0
			self.timeToMoveForward = 1.7
			self.defaultForward = 1.7
			rows = level[1]
			col = level[2]
			for i in range(col):
				self.activeColumns.append(i)
			self.columns = col
			for i in range(rows):
				y = 500 - (40 * i)
				for j in range(col):
					x = (400 - 20 * col) + 40 * j
					if i < level[3]:
						self.array.append(alien.Alien(x, y, batch, value=3))
					elif i < level[4]:
						self.array.append(alien.Alien(x, y, batch, value=2))
					else:
						self.array.append(alien.Alien(x, y, batch, value=1))
		else:
			moveDir = level[2]
			delta = level[3]
			for i in range(level[1]):
				self.array.append(alien.Alien(390, 480 - 40*i, batch, 4, moveDir))
				self.activeColumns.append(i)
				moveDir = -moveDir
				if moveDir > 0:
					moveDir += delta
				else:
					moveDir -= delta

	def spawnBullet(self, dt, target):
		self.timeToShoot -= dt
		if self.timeToShoot <= 0:
			self.timeToShoot = 2.0
			if self.ai:
				p = self.runAI(dt, target)
			else:
				p = random.choice(self.activeColumns)
				if self.normal:
					for pos in range(len(self.array)-self.columns+p, -1, -self.columns):
						if self.array[pos].alive:
							p = pos
							break
			self.ammo.append(bullet.Bullet(-400.0, self.array[p].x + 20, self.array[p].y - 20, self.array[p].batch))

	def runAI(self, dt, target):
		dif = 800.0
		mindif = 800.0
		p = 0
		for c in self.activeColumns:
			if self.normal:
				for pos in range(len(self.array)-self.columns+c, -1, -self.columns):
					if self.array[pos].alive:
						dif = abs(self.array[pos].x + 20 - target)
						if dif < mindif:
							mindif = dif
							p = pos
			else:
				for pos in self.activeColumns:
					dif = abs(self.array[pos].x + 20 - target)
					if dif < mindif:
						mindif = dif
						p = pos
		return p

	def updateNormal(self, dt):
		moveDown = False
		moveForward = False
		changeDir = False
		game = True
		self.timeToMoveDown -= dt
		if self.timeToMoveDown <= 0:
			self.timeToMoveDown = 10.0
			moveDown = True
		self.timeToMoveForward -= dt
		if self.timeToMoveForward <= 0:
			self.timeToMoveForward = self.defaultForward
			if self.defaultForward > 0.7:
				self.defaultForward -= 0.05
			moveForward = True
			leftEdge = 0
			rightEdge = 0
			for pos in range(self.activeColumns[0], len(self.array), self.columns):
				if self.array[pos].alive:
					leftEdge = self.array[pos].x
					break
			for pos in range(self.activeColumns[-1], len(self.array), self.columns):
				if self.array[pos].alive:
					rightEdge = self.array[pos].x + self.array[pos].width
					break
			if leftEdge <= 40 or rightEdge >= 760:
				changeDir = True
		for c in self.activeColumns:
			i = c
			while i < len(self.array):
				if self.array[i].alive:
					break
				i += self.columns
			else:
				self.activeColumns.remove(c)		
		for i in range(len(self.array)):
			if self.array[i].update(dt, moveDown, moveForward, changeDir):
				game = False
		return game

	def update(self, dt, powerups, target):
		win = True
		self.ufo.update(dt, powerups)
		if not powerup.PowerUp.activePowers[powerup.Powers.FREEZE.value]:
			self.spawnBullet(dt, target)
		if self.normal:
			win = self.updateNormal(dt)
		else:
			for i in range(len(self.array)): #self.activeColumns[:]:
				if not self.array[i].alive and i in self.activeColumns:
					#self.array[i].spawnPowerUp(powerups)
					self.activeColumns.remove(i)
				if self.array[i].update(dt):
					win = False
		return win
		
