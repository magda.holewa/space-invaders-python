from . import gameobject, resources

class Bullet(gameobject.GameObject):
	def __init__(self, velocity, x, y, batch, enemy=True):
		super(Bullet, self).__init__(img=resources.bulletImage, x=x, y=y, batch=batch)
		self.velocityY = velocity
		if enemy:
			self.rotation = 180
	def update(self, dt):
		self.y += self.velocityY * dt
		if not self.alive:
			self.batch = None
