import pyglet

class GameObject(pyglet.sprite.Sprite):
	def __init__(self, *args, **kwargs):
		super(GameObject, self).__init__(*args, **kwargs)
		self.alive = True
		self.value = 0
	def checkCollision(self, obj):
		if self.x+self.image.width >= obj.x and obj.x + obj.image.width >= self.x and self.y+self.image.height >= obj.y and obj.y + obj.image.height >= self.y:
			return True
		else:
			return False
	def doCollision(self,obj):
		if self.checkCollision(obj):
			self.alive = False
			obj.alive = False
			return obj.value
		else:
			return 0
