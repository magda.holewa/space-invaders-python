import pyglet
from enum import Enum
from . import textdata, resources
import os.path

class MState(Enum):
	MAIN = 0
	SUB = 1

class Menu:
	def __init__(self, gameWindow):
		self.mainBatch = pyglet.graphics.Batch()
		self.helpBatch = pyglet.graphics.Batch()
		self.scoreBatch = pyglet.graphics.Batch()
		self.subBatch = None
		self.opt = []
		self.opt.append(pyglet.text.Label('New Game', font_size = 20, x = gameWindow.width//2, anchor_x='center', y = 250, batch = self.mainBatch))
		self.opt.append(pyglet.text.Label('Continue', font_size = 20, x = gameWindow.width//2, anchor_x='center', y = 200, batch = self.mainBatch))
		self.opt.append(pyglet.text.Label('High Scores', font_size = 20, x = gameWindow.width//2, anchor_x='center', y = 150, batch = self.mainBatch))
		self.opt.append(pyglet.text.Label('Help', font_size = 20, x = gameWindow.width//2, anchor_x='center', y = 100, batch = self.mainBatch))
		self.opt.append(pyglet.text.Label('Quit', font_size = 20, x = gameWindow.width//2, anchor_x='center', y = 50, batch = self.mainBatch))
		self.backButton = pyglet.text.Label('Back', font_size = 20, x = gameWindow.width//2, anchor_x='center', y = 50)
		self.backButton.color = (150, 150, 255, 255)
		self.helpContent = pyglet.sprite.Sprite(img=resources.helpImage, x=0, y=0, batch = self.helpBatch)
		self.activeOption = 0
		self.opt[0].color = (150, 150, 255, 255)
		self.state = MState.MAIN
		self.scoreList = []
		self.loadList()

	def loadList(self):
		for i in range(5):
			self.scoreList.append(pyglet.text.Label(str(i+1)+'. 0', font_size = 20, x = 150, y = 250 - 40*i, batch = self.scoreBatch))
		for i in range(5):
			self.scoreList.append(pyglet.text.Label(str(i+1)+'. 0', font_size = 20, x = 550, y = 250 - 40*i, batch = self.scoreBatch))
		self.scoreList[0].color = (211, 173, 68, 255)
		self.scoreList[1].color = (170, 170, 170, 255)
		self.scoreList[2].color = (139, 97, 45, 255)
	def updateList(self):
		for i in range(10):
			if i < len(textdata.scores):
				text = str(textdata.scores[i])
			else:
				text = '0'
			self.scoreList[i].text = str(i+1)+'. '+text
	
	def isSaved(self):
		return os.path.isfile("space.sav")
	
	def activate(self):
		if self.state == MState.SUB:
			self.state = MState.MAIN
			return 0		
		elif self.activeOption == 0:
			return 1
		elif self.activeOption == 1:
			if self.isSaved():
				return 2
			else:
				return 1
		elif self.activeOption == 2:
			self.state = MState.SUB
			self.updateList()
			self.subBatch = self.scoreBatch
		elif self.activeOption == 3:
			self.state = MState.SUB
			self.subBatch = self.helpBatch
			return 3
		elif self.activeOption == 4:
			return -1
		else:
			return 0
