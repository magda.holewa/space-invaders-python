from . import resources
from . import gameobject, bullet

class Player(gameobject.GameObject):
	def __init__(self, x, y, batch):
		super(Player, self).__init__(img=resources.playerImage, x=x, y=y, batch=batch)
		self.velocityX = 0.0
		self.velocityY = 0.0
		self.speed = 300.0
		self.shootTimer = 0.0
		self.shootLimit = 0.3
		self.alertTimer = 0.0
		self.alertLimit = 0.5
		self.lives = 3
		self.score = 0
		self.ammo = []
	def update(self, dt):
		if not self.alive:
			if self.lives > 1:
				self.lives -= 1
				self.alive = True
				self.alertTimer = self.alertLimit
				self.color = (255, 100, 100)
				return True
			else:		
				self.lives = 0			
				#self.batch = None
				return False
		else:
			if self.x < 0:
				self.x = 0
			elif self.x + self.width > 800.0:
				self.x = 800.0 - self.width
			else:		
				self.x += (self.velocityX*dt)
			if self.shootTimer > 0:
				self.shootTimer -= dt
			if self.alertTimer > 0:
				self.alertTimer -= dt
				if self.alertTimer <= 0:
					if self.speed == 300:
						self.color = (255, 255, 255)
					else:
						self.color = (100, 255, 100)
			return True

	def shoot(self):
		if self.shootTimer <= 0:
			self.ammo.append(bullet.Bullet(400.0, self.x + 50, self.y, self.batch, False))
			self.shootTimer = self.shootLimit
			return True
		return False
