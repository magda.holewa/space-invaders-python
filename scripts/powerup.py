import pyglet
from enum import Enum
from . import resources, player, gameobject

class Powers(Enum):
	LIFE = 0
	SPEEDUP = 1
	FREEZE = 2
	BONUS = 3
	SHIELD = 4

class PowerUp(gameobject.GameObject):
	activePowers = [False, False, False, False, False]
	def __init__(self, power, x, y, batch):
		self.power = power
		if power == Powers.LIFE:
			img = resources.heartImage
			self.duration = 0.0
		elif power == Powers.SPEEDUP:
			img = resources.speedImage
			self.duration = 10.0
		elif power == Powers.FREEZE:
			img = resources.clockImage
			self.duration = 3.0
		elif power == Powers.BONUS:
			img = resources.pointsImage
			self.duration = 0.0
		elif power == Powers.SHIELD:
			self.duration = 0.0
			img = resources.newShieldImage
		else:
			img = resources.alienImage
		super(PowerUp, self).__init__(img=img, x=x, y=y, batch=batch)
		self.velocityY = -400.0
	def update(self, dt, player, arr, enemy, shields):
		if self.alive:
			self.y += self.velocityY * dt
			if self.checkCollision(player):
				self.alive = False
				self.activate(player, arr, enemy, shields)
				self.batch = None
			elif self.y + self.height <= 0:
				self.batch = None
				arr.remove(self)
		else:
			self.duration -= dt
			if self.duration < 0:
				self.deactivate(player, enemy)
				arr.remove(self)

	def activate(self, player, arr, enemy, shields):
		if self.power == Powers.LIFE:
			player.lives += 1
			arr.remove(self)
		elif self.power == Powers.BONUS:
			player.score += 50
			arr.remove(self)
		elif self.power == Powers.SHIELD:
			for s in shields:
				s.alive = True
				s.batch = self.batch
				s.power = 5
		elif self.power == Powers.SPEEDUP:
			if not PowerUp.activePowers[Powers.SPEEDUP.value]:
				PowerUp.activePowers[Powers.SPEEDUP.value] = True
				player.speed += 500.0
				player.color = (100, 255, 100)
				if player.velocityX > 0:
					player.velocityX += 500.0
				elif player.velocityX < 0:
					player.velocityX -= 500.0
			else:
				arr.remove(self)
		elif self.power == Powers.FREEZE:
			if not PowerUp.activePowers[Powers.FREEZE.value]:
				PowerUp.activePowers[Powers.FREEZE.value] = True
				for a in enemy.array:
					if a.alive:
						if a.value == 10:
							a.color = (196, 255, 159)
						elif a.value == 20:
							a.color = (255, 255, 150)
						elif a.value == 30:
							a.color = (210, 255, 255)
						else:
							a.color = (200, 200, 255)
			else:
				arr.remove(self)

	def deactivate(self, player, enemy):
		if self.power == Powers.SPEEDUP:
			player.speed -= 500.0
			player.color = (255, 255, 255)
			if player.velocityX > 0:
				player.velocityX -= 500.0
			elif player.velocityX < 0:
				player.velocityX += 500.0
		elif self.power == Powers.FREEZE:
			for a in enemy.array:
				if a.alive:
					if a.value == 10:
						a.color = (96, 250, 59)
					elif a.value == 20:
						a.color = (255, 200, 50)
					elif a.value == 30:
						a.color = (150, 230, 255)
					else:
						a.color = (255, 255, 255)
		PowerUp.activePowers[self.power.value] = False
