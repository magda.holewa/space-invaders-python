import random
from . import resources
from . import gameobject, powerup
random.seed()

class RedAlien(gameobject.GameObject):
	def __init__(self, batch):
		super(RedAlien, self).__init__(img=resources.ufoImage, x=-50, y=540, batch=batch)
		self.velocityX = 50.0
		self.sleepTime = 10.0
		self.value = 50
		self.fromLeft = True
	def update(self, dt, powerups):
		if not self.alive:
			self.alive = True
			if self.fromLeft:
				self.x = 800
			else:
				self.x = -50
			self.sleepTime = 20.0
		if self.sleepTime <= 0:
			if self.x < -50:
				self.velocityX = -self.velocityX
				self.sleepTime = 20.0
				self.x = -50
			elif self.x > 800:
				self.velocityX = -self.velocityX
				self.sleepTime = 20.0
				self.x = 800
			else:
				self.x += dt * self.velocityX
		else:
			self.sleepTime -= dt
	
	def spawnPowerUp(self, powerups):
		if not random.randrange(2):
			powerups.append(powerup.PowerUp(powerup.Powers.SHIELD, self.x, self.y, self.batch))
