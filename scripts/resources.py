import pyglet

pyglet.resource.path = ['./resources'] 
pyglet.resource.reindex()

backImage = pyglet.resource.image("back.png")
playerImage = pyglet.resource.image("player.png")
alienImage = pyglet.resource.image("aliengray.png")
bulletImage = pyglet.resource.image("bullet.png")
shieldImage = pyglet.resource.image("shield.png")
ufoImage = pyglet.resource.image("ufo.png")
titleImage = pyglet.resource.image("title.png")
helpImage = pyglet.resource.image("help.png")
stripeImage = pyglet.resource.image("stripe.png")
heartImage = pyglet.resource.image("heart.png")
speedImage = pyglet.resource.image("speed.png")
clockImage = pyglet.resource.image("clock.png")
pointsImage = pyglet.resource.image("point.png")
newShieldImage = pyglet.resource.image("newshield.png")

beam = pyglet.media.load("./resources/laser.wav", streaming = False)
track = pyglet.media.load("./resources/phantomfromspace.wav")

class Level:
	def __init__(foo):
		self.constr = foo
		self.special = True
	def __init__(rows, col):
		pass

levels = []

anlevel = (True, 4, 5, 1, 2)
levels.append(anlevel)
anlevel = (True, 5, 7, 1, 2)
levels.append(anlevel)
anlevel = (False, 5, 2, 1)
levels.append(anlevel)
anlevel = (True, 7, 9, 2, 4)
levels.append(anlevel)
anlevel = (True, 8, 10, 2, 5)
levels.append(anlevel)
anlevel = (False, 8, 10, -0.5)
levels.append(anlevel)

