from . import gameobject, resources

class Shield(gameobject.GameObject):
	def __init__(self, x, y, batch):
		super(Shield, self).__init__(img=resources.shieldImage, x=x, y=y, batch=batch)
		self.power = 5
	def update(self, dt):
		if self.power <= 0:
			self.batch = None
			self.alive = False
		else:
			self.opacity = 255 * self.power * 0.2
