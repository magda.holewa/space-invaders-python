scores = []

def saveGame(level, score, lives):
	f = open('space.sav', 'w')
	f.write(str(level) + '\n')
	f.write(str(score) + '\n')
	f.write(str(lives) + '\n')
	f.close()

def loadGame():
	f = open('space.sav', 'r')
	data = f.readlines()
	for i in range(len(data)):
		data[i] = data[i][:-1]
	f.close()
	return data

def loadScores():
	global scores
	f = open('scores.xd', 'r')
	scores = f.readlines()
	if len(scores):
		for i in range(len(scores)):
			scores[i] = int(scores[i][:-1])
	else:
		scores.append(0)
	f.close()

def saveScores():
	f = open('scores.xd', 'w')
	for s in scores:
		f.write(str(s) + '\n')
	f.close()

def addScore(score):
	if score > scores[-1]:
		for i in range(len(scores)):
			if score > scores[i]:
				scores.insert(i, score)
				break
		if len(scores) > 10:
			scores.pop()
		saveScores()

